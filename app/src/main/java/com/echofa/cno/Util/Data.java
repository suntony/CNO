package com.echofa.cno.Util;

import android.util.Log;

import com.accloud.cloudservice.AC;
import com.accloud.cloudservice.PayloadCallback;
import com.accloud.service.ACException;
import com.accloud.service.ACMsg;
import com.accloud.service.ACObject;

import java.util.ArrayList;

/**
 * Created by SYN on 2017/12/14.
 */
public class Data {

    public  ArrayList<ACObject> userData;
    public  ArrayList<Integer> groupIdData;
    public  ArrayList<ACObject> deviceData;
    public  ArrayList<ACObject> testdata=new ArrayList<>();


    public ArrayList<ACObject> getUserData() {
                return userData;
    }

    public void setUserData(ArrayList<ACObject> userData) {

        this.userData = userData;
    }



    public ArrayList<ACObject> getData(String ServiceName, ACMsg req) {

        AC.sendToService(ServiceName, 2, req, new PayloadCallback<ACMsg>() {
            @Override
            public void success(ACMsg acMsg) {

                userData = (ArrayList<ACObject>) acMsg.get("deviceList"); //所有用户
                for(int i=0;i<userData.size();i++)
                    testdata.add(userData.get(i));

                Log.i("mytag", "获取设备成功"+userData);
                Log.i("mytag", "获取设备成功2"+testdata);
                userData=testdata;
              //  groupIdData = (ArrayList<Integer>)acMsg.get("groupIds");  //获取所登录用户所在的所有分组
              //  deviceData = (ArrayList<ACObject>) acMsg.get("userList"); //设备列表数据，包括设备Id 和 设备所对应的组号
            }

            @Override
            public void error(ACException e) {
                Log.i("mytag", "获取设备失败"+e);
            }
        });
        Log.i("mytag", "获取设备成功3"+testdata);
        return userData;
    }
}
