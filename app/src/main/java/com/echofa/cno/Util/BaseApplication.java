package com.echofa.cno.Util;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.accloud.cloudservice.AC;
import com.accloud.cloudservice.PayloadCallback;
import com.accloud.service.ACException;
import com.accloud.service.ACMsg;
import com.accloud.service.ACObject;

import java.util.ArrayList;

/**
 * Created by SYN on 2017/12/13.
 */
public class BaseApplication extends Application{
    private final String MAJOR_DOMAIN = "acofa";
    private final int MAJOR_DOMAIN_ID = 5613;

    private String userAccount;
    public boolean flag = true;


    private static Context context;
    private static BaseApplication mInstance;

    public String getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(String userAccount) {
        this.userAccount = userAccount;
    }



    @Override
    public void onCreate() {
        super.onCreate();
        AC.init(this, MAJOR_DOMAIN, MAJOR_DOMAIN_ID, AC.TEST_MODE);

    }



    public static BaseApplication getmInstance(){
        return mInstance;
    }

    public static Context getContext(){

        return context;
    }


}
