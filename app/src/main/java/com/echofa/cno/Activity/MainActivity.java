package com.echofa.cno.Activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.echofa.cno.Fragment.ChartFragment;
import com.echofa.cno.Fragment.DeviceFragment;
import com.echofa.cno.Fragment.GroupFragment;
import com.echofa.cno.Fragment.MessageFragment;
import com.echofa.cno.R;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private ViewPager viewPager;
    private TextView tvtitle,tvgroup,tvdevice,tvmessage,tvchart;



    private ArrayList<Fragment> fragments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        InitUI();
    }

    private void InitUI(){
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tvtitle = (TextView) findViewById(R.id.tv_title);
        tvgroup = (TextView) findViewById(R.id.tv_subtitle_group);
        tvdevice = (TextView) findViewById(R.id.tv_subtitle_device);
        tvmessage = (TextView) findViewById(R.id.tv_subtitle_message);
        tvchart = (TextView) findViewById(R.id.tv_subtitle_chart);

        tvgroup.setOnClickListener(this);
        tvdevice.setOnClickListener(this);
        tvmessage.setOnClickListener(this);
        tvchart.setOnClickListener(this);

        changeTitle();
        changeSubtitle();

        fragments = new ArrayList<>();
        fragments.add(new GroupFragment());
        fragments.add(new DeviceFragment());
        fragments.add(new MessageFragment());
        fragments.add(new ChartFragment());

        viewPager.setAdapter(new FragmentAdapter(getSupportFragmentManager(),fragments));
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {

            }

            @Override
            public void onPageSelected(int position) {
                changeTitle();
                changeSubtitle();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


    }

    private void changeTitle(){
        String str="";
        switch (viewPager.getCurrentItem()){
            case 0:
                str=getString(R.string.tv_title_group);
                break;
            case 1:
                str=getString(R.string.tv_title_device);
                break;
            case 2:
                str=getString(R.string.tv_title_message);
                break;
            case 3:
                str=getString(R.string.tv_title_chart);
                break;
        }
        tvtitle.setText(str);
    }

    private void changeSubtitle(){
        tvgroup.setTextColor(Color.parseColor("#666666"));
        tvdevice.setTextColor(Color.parseColor("#666666"));
        tvmessage.setTextColor(Color.parseColor("#666666"));
        tvchart.setTextColor(Color.parseColor("#666666"));

        switch (viewPager.getCurrentItem()){
            case 0:
                tvgroup.setTextColor(Color.parseColor("#0000ff"));
                break;
            case 1:
                tvdevice.setTextColor(Color.parseColor("#0000ff"));
                break;
            case 2:
                tvmessage.setTextColor(Color.parseColor("#0000ff"));
                break;
            case 3:
                tvchart.setTextColor(Color.parseColor("#0000ff"));
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tv_subtitle_group:
                viewPager.setCurrentItem(0);
                break;
            case R.id.tv_subtitle_device:
                viewPager.setCurrentItem(1);
                break;
            case R.id.tv_subtitle_message:
                viewPager.setCurrentItem(2);
                break;
            case R.id.tv_subtitle_chart:
                viewPager.setCurrentItem(3);
                break;
        }
        changeTitle();
        changeSubtitle();
    }

    final private class FragmentAdapter extends FragmentPagerAdapter{

        private ArrayList<Fragment> fragments;

        public FragmentAdapter(FragmentManager fm,ArrayList<Fragment> fragments) {
            super(fm);
            this.fragments = fragments;
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }
    }
}
