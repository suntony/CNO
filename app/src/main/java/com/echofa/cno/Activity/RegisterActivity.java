package com.echofa.cno.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.accloud.cloudservice.AC;
import com.accloud.cloudservice.PayloadCallback;
import com.accloud.cloudservice.VoidCallback;
import com.accloud.service.ACAccountMgr;
import com.accloud.service.ACException;
import com.accloud.service.ACUserInfo;
import com.echofa.cno.R;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText register_account;
    private EditText register_password;
    private EditText register_verifyCode;

    private ImageView back_to_login;
    private Button register;
    private Button btnVerifyCode;

    private ACAccountMgr accountMgr;
    private String account;
    private String password;
    private String verifyCode;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        init();
        initView();
    }

    private void init() {
        accountMgr = AC.accountMgr();
    }

    private void initView() {
        register_account = (EditText) findViewById(R.id.et_register_account);
        register_password = (EditText) findViewById(R.id.et_register_password);
        register_verifyCode = (EditText) findViewById(R.id.et_register_verifyCode);
        back_to_login = (ImageView) findViewById(R.id.back_to_login);
        register = (Button) findViewById(R.id.btn_register);
        btnVerifyCode = (Button) findViewById(R.id.btn_register_verifyCode);

        back_to_login.setOnClickListener(this);
        register.setOnClickListener(this);
        btnVerifyCode.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        account = register_account.getText().toString();
        password = register_password.getText().toString();
        verifyCode = register_verifyCode.getText().toString();
        switch (view.getId()){
            case R.id.back_to_login:
                finish();
                break;
            case R.id.btn_register:

                checkAccountExist(account, password, verifyCode);
                break;
            case R.id.btn_register_verifyCode:
                sendVerifyCode(account);
                break;

        }
    }

    private void checkAccountExist(String account, String password, String verifyCode) {
        final String send_account = account;
        final String send_password = password;
        final String send_verifyCode = verifyCode;

        accountMgr.checkExist(account, new PayloadCallback<Boolean>() {
            @Override
            public void success(Boolean isExist) {
                if(!isExist){
                    checkVerifyCode(send_account, send_password, send_verifyCode);
                    Log.d("tag","账号不存在");
                }else {
                    register_account.setText(send_account);
                    Log.d("tag","账号已经存在");
                }

                register_account.setText(send_account);
                register_password.setText(send_password);
            }
            @Override
            public void error(ACException e) {

            }
        });
    }

    private void sendVerifyCode(String send_account){
            accountMgr.sendVerifyCode(send_account, 1, new VoidCallback() {
            @Override
            public void success() {
                Log.d("tag", "验证码发送成功");
                checkVerifyCode(account, password, verifyCode);
            }

            @Override
            public void error(ACException e) {
                Log.d("tag", "验证码发送失败");
            }
        });
    }

    private void checkVerifyCode(String account, String password, String verifyCode) {
        final String regis_account = account;
        final String regis_password = password;
        final String regis_verifyCode = verifyCode;

        accountMgr.checkVerifyCode(account, verifyCode, new PayloadCallback<Boolean>() {
            @Override
            public void success(Boolean result) {
                if(result){
                    Log.d("tag" , "验证码正确");
                    register(regis_account, regis_password, regis_verifyCode);
                }
            }

            @Override
            public void error(ACException e) {
                Log.d("tag", "检验验证码失败");
                register_account.setText(regis_account);
                register_password.setText(regis_password);
            }
        });
    }

    private void register(final String account, final String password, String verifyCode) {
        final String regis_account = account;
        final String regis_password = password;
        final String regis_verifyCode = verifyCode;
        accountMgr.register(" ", regis_account, regis_password, " ", regis_verifyCode, new PayloadCallback<ACUserInfo>() {
            @Override
            public void success(ACUserInfo acUserInfo) {
                Log.d("tag", "注册成功");
                register_account.setText(regis_account);
                register_password.setText(regis_password);
            }
            @Override
            public void error(ACException e) {
                register_account.setText(regis_account);
                register_password.setText(regis_password);
                Log.d("tag", "注册成功");
            }
        });
    }
}
