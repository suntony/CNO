package com.echofa.cno.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.accloud.cloudservice.AC;
import com.accloud.cloudservice.PayloadCallback;
import com.accloud.service.ACAccountMgr;
import com.accloud.service.ACException;
import com.accloud.service.ACMsg;
import com.accloud.service.ACObject;
import com.accloud.service.ACProduct;
import com.accloud.service.ACUserInfo;
import com.echofa.cno.R;
import com.echofa.cno.Util.BaseApplication;
import com.echofa.cno.Util.Data;

import java.util.ArrayList;
import java.util.List;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText loginPhone;
    private EditText loginPassword;
    private Button login;
    private TextView register;

    private ACAccountMgr accountMgr;

    private Button jump;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();
        initView();

    }

    private void init() {
        accountMgr = AC.accountMgr();
            /*if(accountMgr.isLogin()){

            Intent intent = new Intent(LoginActivity.this,MainActivity.class);
            startActivity(intent);
            return;
        }*/

    }

    private void initView() {
        loginPhone = (EditText) findViewById(R.id.edit_login_phone);
        loginPassword = (EditText) findViewById(R.id.edit_login_password);
        login = (Button) findViewById(R.id.btn_login);
        register = (TextView) findViewById(R.id.tv_register);
        jump = (Button) findViewById(R.id.jump);

        loginPhone.setOnClickListener(this);
        loginPassword.setOnClickListener(this);
        login.setOnClickListener(this);
        register.setOnClickListener(this);
        jump.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_login:
                //测试
                Data data = new Data();
                ACMsg req = new ACMsg();
                req.setName("getDeviceList");
                req.put("username", "18251887993");
               /* while(((BaseApplication)getApplication()).flag) {
                    Log.i("mytag", "数据信息" + data.getData("DEVICEDEMO", req));
                }
*/
                String account = loginPhone.getText().toString();
                String password = loginPassword.getText().toString();
                accountMgr.login(account, password, new PayloadCallback<ACUserInfo>() {
                    @Override
                    public void success(ACUserInfo acUserInfo) {
                       ((BaseApplication)getApplication()).setUserAccount(acUserInfo.getPhone().toString());
                        Log.i("mytag","用户账号"+acUserInfo.getPhone().toString());

                        Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                        startActivity(intent);
                    }
                    @Override
                    public void error(ACException e) {

                    }
                });

            break;
            case R.id.tv_register:
               Intent intent = new Intent(LoginActivity.this,RegisterActivity.class);
                startActivity(intent);
                break;

            case R.id.jump:
               Intent intent1 = new Intent(LoginActivity.this,MainActivity.class);
                startActivity(intent1);
                break;
        }

    }
}
